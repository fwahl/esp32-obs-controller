/**
 * This example turns the ESP32 into a Bluetooth LE keyboard that writes the words, presses Enter, presses a media key and then Ctrl+Alt+Delete
 */
#include <BleKeyboard.h>

const int button1Pin = 13;
const int button2Pin = 12;
const int button3Pin = 14;
const int button4Pin = 27;
const int button5Pin = 26;
const int button6Pin = 25;
const int button7Pin = 33;
const int button8Pin = 32;

bool button1Fired = false;
bool button2Fired = false;
bool button3Fired = false;
bool button4Fired = false;
bool button5Fired = false;
bool button6Fired = false;
bool button7Fired = false;
bool button8Fired = false;

BleKeyboard bleKeyboard("OBS Controller", "wahlytics", 100);

void setup() {
  pinMode(button1Pin, INPUT);
  pinMode(button2Pin, INPUT);
  pinMode(button3Pin, INPUT);
  pinMode(button4Pin, INPUT);
//  pinMode(button5Pin, INPUT);
//  pinMode(button6Pin, INPUT);
//  pinMode(button7Pin, INPUT);
//  pinMode(button8Pin, INPUT);
  
  attachInterrupt(button1Pin, button1ISR, FALLING);
  attachInterrupt(button2Pin, button2ISR, FALLING);
  attachInterrupt(button3Pin, button3ISR, FALLING);
  attachInterrupt(button4Pin, button4ISR, FALLING);
  attachInterrupt(button5Pin, button5ISR, FALLING);
  attachInterrupt(button6Pin, button6ISR, FALLING);
  attachInterrupt(button7Pin, button7ISR, FALLING);
  attachInterrupt(button8Pin, button8ISR, FALLING);
  
  bleKeyboard.begin();
}

void loop() {
  if(bleKeyboard.isConnected()) {
    if(button1Fired) {
      handleButton(KEY_F12);
      button1Fired = false;
    }

    if(button2Fired) {
      handleButton(KEY_F11);
      button2Fired = false;
    }

    if(button3Fired) {
      handleButton(KEY_F10);
      button3Fired = false;
    }

    if(button4Fired) {
      handleButton(KEY_F9);
      button4Fired = false;
    }

    if(button5Fired) {
      handleButton(KEY_F8);
      button5Fired = false;
    }

    if(button6Fired) {
      handleButton(KEY_F7);
      button6Fired = false;
    }

    if(button7Fired) {
      handleButton(KEY_F6);
      button7Fired = false;
    }

    if(button8Fired) {
      handleButton(KEY_F4);
      button8Fired = false;
    }    
  }
}

void handleButton(int fkey) {
  noInterrupts();
  bleKeyboard.press(KEY_LEFT_SHIFT);
  bleKeyboard.press(fkey);
  delay(200);
  bleKeyboard.releaseAll();
  interrupts();
}

void button1ISR() {
  button1Fired = true;
}

void button2ISR() {
  button2Fired = true;
}

void button3ISR() {
  button3Fired = true;
}

void button4ISR() {
  button4Fired = true;
}

void button5ISR() {
  button5Fired = true;
}

void button6ISR() {
  button6Fired = true;
}

void button7ISR() {
  button7Fired = true;
}

void button8ISR() {
  button8Fired = true;
}
